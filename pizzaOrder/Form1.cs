﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pizzaOrder
{
    public partial class Form1 : Form
    {
        double price = 10;
        double chickenPrice = 2;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int quantity = Convert.ToInt32(textBox1.Text);
            double total;
            if (checkBox1.Checked == true)
            {
                 total = (quantity * price) + (quantity * chickenPrice);
                Console.WriteLine(total);
                label3.Text = "Pizza Ordered!!!!";
                label4.Text = "Total amount is : $" + total;
            }
            else
            {
                total = quantity * price;
                Console.WriteLine(total);
                label3.Text = "Pizza Ordered!!!!";
                label4.Text = "Total amount is : $" + total;
            }
        }
    }
}
